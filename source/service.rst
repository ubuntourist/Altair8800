.. include:: /_static/isonum.txt

.. role:: raw-html(raw)
   :format: html

#######
SERVICE
#######

Should you have a problem with your computer, it can be returned to
MITS for repair. If the unit is stil] under warranty, any defective
part will be replaced free of charge. The purchaser is responsible for
all postage.  In no case should a unit be shipped back without the
outer case fully assembled.

If you need to return the unit to us for any reason, remove the top
cover of your computer and secure the cards in their sockets with tape
and fill the space between the case top and the cards with packing
material. Secure cover and pack the unit in a sturdy cardboard
container and surround it on all sides with a thick layer of packing
material. You can use shredded newspaper, foamed plastic or excelsior.
The packed carton should be neatly sealed with gummed tape and tied
with a stout cord. Be sure to tape a letter containing your name and
address, a description of the malfunction, and the original invoice
(if the unit is still under warranty) to the outside of the box.

Mail the carton by parcel post or UPS\ |--|\ for extra fast service,
ship by air parcel post. Be sure to insure the package.

.. table::
   :column-alignment: left left

   +----------+----------------------------------------+
   | SHIP TO: | MITS, Inc.          :raw-html:`<br />` |
   |          | 6328 Linn Ave. N.E. :raw-html:`<br />` |
   |          | Albuquerque, New Mexico 87108          |
   +----------+----------------------------------------+

All warranties are void if any changes have been made to the basic
design of the machine or if the internal workings have been tampered
with in any way.
