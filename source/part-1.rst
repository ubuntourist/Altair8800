.. include:: /_static/isonum.txt

.. include:: <isoamsa.txt>

.. |opened|    unicode:: U+02197 .. NORTH EAST ARROW
   :trim:

.. |closed|    unicode:: U+1F802 .. RIGHTWARDS ARROW WITH SMALL TRIANGLE ARROWHEAD
   :trim:

.. |wire|      unicode:: U+23AF  .. HORIZONTAL LINE EXTENSION 
   :trim:

.. |AND_gate|   image:: _static/and-gate.svg
   :alt: AND gate
   :width: 32%

.. |OR_gate|    image:: _static/or-gate.svg
   :alt: OR gate
   :width: 32%

.. |NOT_gate|   image:: _static/not-gate.svg
   :alt: NOT gate
   :width: 32%

.. |XOR_gate|    image:: _static/xor-gate.svg
   :alt: OR gate
   :width: 32%

.. |NAND_gate|  image:: _static/nand-gate.svg
   :alt: AND gate
   :width: 32%

.. |NOR_gate|   image:: _static/nor-gate.svg
   :alt: OR gate
   :width: 32%

.. |half_adder| image:: _static/half-adder.svg
   :alt: Exclusive OR (XOR) gate / Half-adder

.. |00| image:: _static/logic-switches.00.svg
   :alt: Off (0) AND Off (0)

.. |01| image:: _static/logic-switches.01.svg
   :alt: Off (0) AND On (1)

.. |10| image:: _static/logic-switches.10.svg
   :alt: On (1) AND Off (0)

.. |11| image:: _static/logic-switches.11.svg
   :alt: On (1) AND On (1)

.. role:: raw-html(raw)
   :format: html

####################
PART 1: INTRODUCTION
####################

Remarkable advances in semiconductor technology have made possible the
development of the *ALTAIR 8800*, the most economical computer ever and
the first available in both kit and assembled form. The heart of the
*ALTAIR 8800* is Intel Corporation's Model 8080 Microcomputer, a
complete Central Processing Unit on a single silicon chip. Fabricated
with N-channel large scale integrated circuit (LSI)
metal-oxide-semiconductor (MOS) technology, Intel's 8080 Microcomputer
on a chip represents a major technological breakthrough.

This operating manual has been prepared to acquaint both the novice
and the experienced computer user in the operation of the *ALTAIR 8800*.
The computer has 78 machine language instructions and is capable of
performing several important operations not normally available with
conventional mini-computers. After reading this manual, even a novice
will be able to load a program into the *ALTAIR 8800*.

Users of the *ALTAIR 8800* include persons with a strong electronics
background and little or no computer experience and persons with
considerable programming experience and little or no electronics
background. Accordingly, this manual has been prepared with all types
of users in mind.

`Part 1`_ of the manual prepares the user for better understanding
computer terminology, technology, and operation with an introduction
to conventional and electronic logic, a description of several
important number systems, a discussion of basic programming, and a
discourse on computer languages.

`Part 2`_ and `Part 3`_ in the manual describe the organization and
operation of the *ALTAIR 8800*. Emphasis is placed on those portions
of the computer most frequently utilized by the user. Finally, `Part
4`_ of the manual presents a detailed listing of the *ALTAIR 8800*'s
78 instructions. An `Appendix`_ condenses the instructions into a quick
reference listing.

Even if you have little or no experience in computer operation and
organization, a careful reading of this manual will prepare you for
operating the *ALTAIR 8800*. As you gain experience with the machine,
you will soon come to understand its truly incredible versatility and
data processing capability. Don't be discouraged if the manual seems
too complicated in places. Just remember that a computer does only
what its programmer instructs it to do.

A. LOGIC
========

George Boole, a ninteenth century British mathematician, made a
detailed study of the relationship between certain fundamental logical
expressions and their arithmetic counterparts. Boole did not equate
mathematics with logic, but he did show how any logical statement can
be analyzed with simple arithmetic relationships. In 1847, Boole
published a booklet entitled `The Mathematical Analysis of Logic`_ and
in 1854 he published a much more detailed work on the subject.  To
this day, all practical digital computers and many other electronic
circuits are based upon the logic concepts explained by Boole.

Boole's system of logic, which is frequently called Boolean algebra,
assumes that a logic condition or statement is either true or
false. It cannot be both true and false, and it cannot be partially
true or partially false. Fortunately, electronic circuits are
admirably suited for this type of dual-state operation. If a circuit
in the ON state is said to be true and a circuit in the OFF state is
said to be false, an electronic analogy of a logical statement can be
readily synthesized.

With this in mind, it is possible to devise electronic equivalents for
the three basic logic statements: AND, OR and NOT. The AND statement
is true only if **all** of its logic conditions are true. An OR
statement is true if **any** of its logic conditions is true. A NOT
statement merely reverses the meaning of a logic statement so that a
true statement is false and a false statement is true.

It's easy to generate a simple equivalent of these three logic
statements by using on-off switches. A switch which is ON is said to
be true while a switch which is OFF is said to be false. Since a
switch which is OFF will not pass an electrical current, it can be
assigned a numerical value of 0. Similarly, a switch which is ON does
pass an electrical current and can be assigned a numerical value of 1.

We can now devise an electronic equivalent of the logical AND
statement by examining the various permutations for a two condition
AND statement:

.. container:: center

   .. table::
      :widths: 1 1 1 3
      :header-alignment: center center
      :column-alignment: center center center center
      :column-dividers: single none none single single

      +---------------------+------------+
      |     CONDITIONS      | CONCLUSION |
      |      (Inputs)       |  (Output)  |
      +=======+=====+=======+============+
      | False | AND | False |   False    |
      +-------+-----+-------+------------+
      | False | AND | True  |   False    |
      +-------+-----+-------+------------+
      | True  | AND | False |   False    |
      +-------+-----+-------+------------+
      | True  | AND | True  |   True     |
      +-------+-----+-------+------------+

The electronic ON-OFF switch equivalent of these permutations is simply:

.. container:: center

   .. table::
      :widths: 3 1
      :header-alignment: center center
      :column-alignment: center center 
      :column-dividers: single single single

      +-------------+----------------------+
      |  Conditions |     Conclusion       |
      +-------------+----------------------+
      |   (OFF-ON)  |      (Output)        |
      +=============+======================+
      |    |00|     | :raw-html:`<br />` 0 |
      +-------------+----------------------+
      |    |01|     | :raw-html:`<br />` 0 |
      +-------------+----------------------+
      |    |10|     | :raw-html:`<br />` 0 |
      +-------------+----------------------+
      |    |11|     | :raw-html:`<br />` 1 |
      +-------------+----------------------+

Similarly, the numerical equivalents of these permutations is:

.. container:: center

   .. table::
      :widths: 1 1 1 3
      :header-alignment: center center
      :column-alignment: center center center center
      :column-dividers: single none none single single

      +-------------+------------+
      | CONDITIONS  | CONCLUSION |
      |  (Inputs)   |  (Output)  |
      +===+=====+===+============+
      | 0 | AND | 0 |     0      |
      +---+-----+---+------------+
      | 0 | AND | 1 |     0      |
      +---+-----+---+------------+
      | 1 | AND | 0 |     0      |
      +---+-----+---+------------+
      | 1 | AND | 1 |     1      |
      +---+-----+---+------------+

Digital design engineers refer to these table of permutations as truth
tables. The truth table for the AND statement with two Conditions 1s
usually presented thusly:

.. container:: center

   *FIGURE 1-1. AND Function Truth Table*

   .. table::
      :header-alignment: center center center
      :column-alignment: center center center
      :column-dividers: single single single single

      = = ===
      A B OUT
      = = ===
      0 0  0
      0 1  0
      1 0  0
      1 1  1
      = = ===

It is now possible to derive the truth tables for the OR and NOT
statements, and each is shown in Figures 1-2 and 1-3 respectively.

.. container:: center

   *FIGURE 1-2. OR Function Truth Table*

   .. table::
      :header-alignment: center center center
      :column-alignment: center center center
      :column-dividers: single single single single

      = = ===
      A B OUT
      = = ===
      0 0  0
      0 1  1
      1 0  1
      1 1  1
      = = ===

.. container:: center

   *FIGURE 1-3. NOT Function Truth Table*

   .. table::
      :header-alignment: center center
      :column-alignment: center center
      :column-dividers: single single single

      = ===
      A OUT
      = ===
      0  1
      1  0
      = ===

B. ELECTRONIC LOGIC
===================

All three of the basic logic functions can be implemented by relatively
simple transistor circuits. By convention, each circuit has been
assigned a symbol to assist in designing logic systems. The three
symbols along with their respective truth tables are shown in Figure
1-4.

|AND_gate| |OR_gate| |NOT_gate|

.. container:: center

   .. table:: FIGURE 1-4. The Three Main Logic Symbols
      :widths: 3 1 2 3 1 2 3 1
      :header-alignment: center center center center center center center center
      :column-alignment: center center center center center center center center
      :column-dividers: single none single double none single double none single

      +-------------+-------+-----+---------+
      |    AND      |     OR      |   NOT   |
      +---+---+-----+---+---+-----+---+-----+
      | A | B | OUT | A | B | OUT | A | OUT |
      +===+===+=====+===+===+=====+===+=====+
      | 0 | 0 |  0  | 0 | 0 |  0  | 0 |  1  |
      +---+---+-----+---+---+-----+---+-----+
      | 0 | 1 |  0  | 0 | 1 |  1  | 1 |  0  |
      +---+---+-----+---+---+-----+---+-----+
      | 1 | 0 |  0  | 1 | 0 |  1  |         |
      +---+---+-----+---+---+-----+         |
      | 1 | 1 |  1  | 1 | 1 |  1  |         |
      +---+---+-----+---+---+-----+---+-----+

The three basic logic circuits can be combined with one another to
produce still more logic statement analogies. Two of these circuit
combinations are used so frequently that they are considered basic
logic circuits and have been assigned their own logic symbols and
truth tables. These circuits are the NAND (NOT-AND) and the NOR
(NOT-OR). Figure 1-5 shows the logic symbols and truth tables for
these circuits.

|NAND_gate| |NOR_gate|

.. container:: center

   *FIGURE 1-5. The NAND and NOR Circuits*

   .. table::
      :widths: 3 1 2 3 1 2
      :header-alignment: center center center center center center
      :column-alignment: center center center center center center
      :column-dividers: single none single double none single single

      +-------------+-------+-----+
      |    NAND     |     NOR     |
      +---+---+-----+---+---+-----+
      | A | B | OUT | A | B | OUT |
      +===+===+=====+===+===+=====+
      | 0 | 0 |  1  | 0 | 0 |  1  |
      +---+---+-----+---+---+-----+
      | 0 | 1 |  1  | 0 | 1 |  0  |
      +---+---+-----+---+---+-----+
      | 1 | 0 |  1  | 1 | 0 |  0  |
      +---+---+-----+---+---+-----+
      | 1 | 1 |  0  | 1 | 1 |  0  |
      +---+---+-----+---+---+-----+

Three or more logic circuits make a logic system. One of the most
basic logic systems is the EXCLUSIVE-OR circuit shown in Figure 1-6.

*FIGURE 1-6. The EXCLUSIVE-OR Circuit*

|half_adder|

.. container:: center

   .. table::
      :header-alignment: center center center center center center
      :column-alignment: center center center center center center
      :column-dividers: single single double double double double single

      +---+---+---------+---------------+--------+------------------------------+
      | A | B | A and B | not (A and B) | A or B | (not (A and B) and (A or B)) |
      +---+---+---------+---------------+--------+------------------------------+
      |       | CARRY   |                        |        OUT a.k.a. SUM        |
      +===+===+=========+===============+========+==============================+
      | 0 | 0 |    0    |       1       |   0    |              0               |
      +---+---+---------+---------------+--------+------------------------------+
      | 0 | 1 |    0    |       1       |   1    |              1               |
      +---+---+---------+---------------+--------+------------------------------+
      | 1 | 0 |    0    |       1       |   1    |              1               |
      +---+---+---------+---------------+--------+------------------------------+
      | 1 | 1 |    1    |       0       |   1    |              0               |
      +---+---+---------+---------------+--------+------------------------------+

The EXCLUSIVE-OR circuit can be used to implement logical functions,
but it can also be used to add two input conditions. Since electronic
logic circuits utilize only two numerical units, 0 and 1, they are
compatible with the binary number system, a number system which has
only two digits.  For this reason, the EXCLUSIVE-OR circuit is often
called a binary adder.

Examining only the CARRY and SUM columns from Figure 1-6 combined as
if they were a single 2-digit binary number yields:

.. container:: center

   .. table::
      :header-alignment: center center center center center
      :column-alignment: center center center center center
      :column-dividers: single single double single single single

      +---+---+----------------------+
      | A | B |         A + B        |
      +---+---+-------+-----+--------+
      |       | CARRY | SUM | RESULT |
      +===+===+=======+=====+========+
      | 0 | 0 |   0   |  0  |   00   |
      +---+---+-------+-----+--------+
      | 0 | 1 |   0   |  1  |   01   |
      +---+---+-------+-----+--------+
      | 1 | 0 |   0   |  1  |   01   |
      +---+---+-------+-----+--------+
      | 1 | 1 |   1   |  0  |   10   |
      +---+---+-------+-----+--------+

Typically, when referring to an Exclsive OR (XOR), the CARRY is ignored,
and it is graphically represented as:

|XOR_gate|

Various combinations of logic circuits can be used to implement
numerous electronic functions. For example, two NAND circuits can be
connected to form a bistable circuit called a flip-flop. Since the
flip-flop changes state only when an incoming signal in the form of a
pulse arrives, it acts as a short term memory element. Several
flip-flops can be cascaded together to form electronic counters and
memory registers.

Other logic circuits can be connected together to form monostable and
astable circuits. Monostable circuits occupy one of two states unless
an incoming pulse is received.  They then occupy an opposite state for
a brief time and then resume their normal state. Astable circuits
continually switch back and forth between two states.

C. NUMBER SYSTEMS
=================

Probably because they found it convenient to count with their fingers,
early humans devised a number system which consisted of ten digits.
Number systems, however, can be based on any number of digits. As we
have already seen, dual-state electronic circuits are highly
compatible with a two digit number system, and its digits are termed
**bits** (**b**\ inary dig\ **its**).  Systems based upon eight and
sixteen are also compatible with complex electronic logic systems such
as computers since they provide a convenient shorthand method for
expressing lengthy binary numbers.

D. THE BINARY SYSTEM
====================

Like virtually all digital computers, the *ALTAIR 8800* performs nearly
all operations in binary. A typical binary number processed by the
computer incorporates 8-bits and may appear as: 10111010. A fixed
length binary number such as this is usually called a word or byte,
and computers are usually designed to process and store a fixed number
of words (or bytes).

A binary word like 10111010 appears totally meaningless to the
novice. But since binary utilizes only two digits (bits), it is
actually much simpler than the familiar and traditional decimal
system. To see why, let's derive the binary equivalents for the
decimal numbers from 0 to 20. We will do this by simply adding 1 to
each successive number until all the numbers have been derived.
Counting in any number system is governed by one basic rule:
Record successive digits for each count in a column. When the total
number of available digits has been used, begin a new column to the
left of the first and resume counting.

Counting from 0 to 20 in binary is very easy since there are only two
digits (bits). The binary equivalent of the decimal 0 is 0. Similarly,
the binary equivalent of the decimal 1 is 1. Since both available bits
have now been used, the binary count must incorporate a new column to
form the binary equivalent for the decimal 2. The result is 10.
(Incidentally, ignore any resemblance between binary and decimal
numbers. Binary 10 is **not** decimal 10!) The binary equivalent of
the decimal number 3 is 11. Both bits have been used again, so a third
column must be started to obtain the binary equivalent for the decimal
number 4 (100). You should now be able to continue counting and derive
all the remaining binary equivalents for the decimal numbers 0 to 20:

.. container:: center

   .. table::
      :header-alignment: center center
      :column-alignment: center right
      :column-dividers: single none single
   
      ======= ======
      DECIMAL BINARY
      ======= ======
         0         0
         1         1
         2        10
         3        11
         4       100
         5       101
         6       110
         7       111
         8      1000
         9      1001
        10      1010
        11      1011
        12      1100
        13      1101
        14      1110
        15      1111
        16     10000
        17     10001
        18     10010
        19     10011
        20     10100
      ======= ======

A simple procedure can be used to convert a binary number into its
decimal equivalent. Each bit in a binary number indicates by which
power of two the number js to be raised.  The sum of the powers of two
gives the decimal equivalent for the number. For example, consider the
binary number 10011:

.. math::
   :nowrap:

   \begin{gather*}
   10011 = [(1 \times 2^4) + (0 \times 2^3) + (0 \times 2^2) + (1 \times 2^1) + (1 \times 2^0)]\\
   = [(16) + (0) + (0) + (2) + (1)]\\
   = 19
   \end{gather*}

E. THE OCTAL SYSTEM
===================

Since the binary system has only two bits, it doesn't take long to
accumulate a long string of Os and 1s. For example, a six-digit
decimal number requires 19 bits.

Lengthy binary numbers can be simplified by dividing them into groups
of three bits and assigning a decimal equivalent to each 3-bit
group. Since the highest 3-bit binary number corresponds to the
decimal 7, eight combinations of 0s and 1s are possible (0-7).

The basic *ALTAIR 8800* accepts a binary input, and any binary number
loaded into the machine can be simplified into octal format. Of course
the octal numbers must be changed back to binary for entry into the
computer, but since only eight bit patterns are involved the procedure
is both simple and fast. A typical binary instruction for the *ALTAIR
8800* is: 11101010. This instruction can be converted to octal by first
dividing the number into groups of three bits beginning with the least
significant bit: 11 101 010.  Next, assign the decimal equivalent to
each of the three bit patterns:

.. container:: center

   .. table::
      :header-alignment: center center center
      :column-alignment: center center center
      :column-dividers: single none none single

      == === ===
      11 101 010
       3  5   2
      == === ===

Therefore, 11 101 010 in binary corresponds to 352 in octal. To permit
rapid binary to octal conversion throughout the remainder of this
manual, most binary numbers will be presented as groups of three bits.

F. COMPUTER PROGRAMMING
=======================

As will become apparent in Part 2, the Central Processing Unit (CPU)
of a computer is essentially a network of logic circuits and systems
whose interconnections or organization can be changed by the user. The
computer can therefore be thought of as a piece of **variable
hardware**. Implementation of variations in a computer's hardware is
achieved with a set of programmed instructions called **software**.

The software instructions for the *ALTAIR 8800* must be loaded into the
machine in the form of sequential 8-bit words called **machine
language**. This and other more advanced computer languages will be
discussed later.

The basics of computer programming are quite simple. In fact, often
the most difficult part of programming is defining the problem you
wish to solve with the computer.  Below are listed the three main
steps in generating a program:

1. Defining the Problem
2. Establishing an Approach
3. Writing the Program

Once the problem has been defined, an approach to its solution can be
developed. This step is simplified by making a diagram which shows the
orderly, step-by-step solution of the problem. Such a diagram is
called a **flow diagram**.  After a flow diagram has been made, the
various steps can be translated into the computer's language. This is
the easiest of the three steps since all you need is a general
understanding of the instructions and a list showing each instruction
and its machine language equivalent.

The *ALTAIR 8800* has an extensive programming capability.  For example,
a program can cause data to be transferred between the computer's
memory and the CPU. The program can even cause the computer to make
logical decisions. For example, if a specified condition is met, the
computer can jump from one place in the program to any other place and
continue program execution at the new place. Frequently used special
purpose programs can be stored in the computer's memory for later
retrieval and use by the main program. Such a special purpose program
is called a subroutine. The *ALTAIR 8800* instructions are described in
detail in Part 4 of this manual.

G. A SIMPLE PROGRAM
===================

Assume you wish to use the *ALTAIR 8800* to add two numbers located at
two different memory locations and store the result elsewhere in the
memory. Of course this is a very simple problem, but it can be used to
illustrate several basic programming techniques. Here are the steps
used in generating a program to solve this problem:

1. Define the Problem\ |--|\ Add two numbers located in memory and store the
   result elsewhere in memory.

2. Establish an Approach\ |--|\ A flow diagram can now be generated:

.. container:: center

   .. table::

      +------------------------------------+
      | .. image:: _static/flowchart-1.svg |
      +------------------------------------+

3. Write the Program\ |--|\ Translating the flow diagram into a
   language or format suitable for use by the computer may seem
   complicated at first. However, a general knowledge of the
   computer's organization and operation makes the job simple. In
   this case, the four part flow diagram translates into five
   separate instructions:

.. container:: center

   .. table::

      +------------------------------------+
      | .. image:: _static/flowchart-2.svg |
      +------------------------------------+

These instructions may seem meaningless now, but their meaning and
application will become much clearer as you proceed through this
manual. For example, the need for the extra instruction (``MOV``) will
become more obvious after you learn that the computer must temporarily
store the first number retrieved from memory in a special CPU memory
called a register. The first number is stored in the register until it
can be added to the second number.

H. COMPUTER LANGUAGES
=====================

The software for any computer must be entered into the machine in the
form of binary words called **machine language**. Machine language
programs are generally written with the help of **mnemonics** which
correspond to the bit patterns for various instructions. For example,
10 000 111 is an add instruction for the *ALTAIR 8800* and the
corresponding mnemonic is ``ADD A``. Obviously the mnemonic ``ADD A``
is much more convenient to remember than its corresponding machine
language bit pattern.

Ultimately, however, the machine language bit pattern for each
instruction must be entered into the computer one step at a time. Some
instructions may require more than one binary word. For example, an
*ALTAIR 8800* instruction which references a memory address such as
``JMP`` requires one word for the actual instruction and two
subsequent words for the memory address.

Machine language programs are normally entered into the *ALTAIR 8800* by
means of the front panel switches. A computer terminal can be used to
send the mnemonics signal 17 to the computer where it is converted
into machine language by a special set of instructions (software)
called an **assembler**.

Even more flexibility is offered by a highly complex software package
called a **compiler** which converts higher order mnemonics into
machine language. Higher order mnemonics are a type of computer
language shorthand which automatically replace as many as a dozen or
more machine language instructions with a single, easily recognized
mnemonic.  Advanced computer languages such as **FORTRAN**, **BASIC**,
**COBOL**, and others make use of a compiler.

The higher computer languages provide a great deal of simplification
when writing computer programs, particularly those that are
lengthy. They are also very easy to remember. The potential
versatility of machine language programming should not be
underestimated, however, and an excellent way to realize the full
potential of a higher language is to learn to apply machine language.

----

.. _Part 1: part-1.html
.. _Part 2: part-2.html
.. _Part 3: part-3.html
.. _Part 4: part-4.html
.. _Appendix: appendix.html
.. _The Mathematical Analysis of Logic: https://www.gutenberg.org/ebooks/36884
