.. include:: <isonum.txt>
.. include:: /_static/isonum.txt
.. |wink| unicode:: U+1F609 .. WINKING FACE

########################################
PART 0: PREFACE: COLOPHON, CREDITS, ETC.
########################################

This labor of love was started by Kevin Cole ("The Ubuntourist") in
September 2020, in an effort to supplement a `Computer Systems
course`_.

Users of this document can test their code with either Ian Davies'
amazing `MITS Altair 8800 simulator`_ or, for mere $621 (US), a
physical `Altair 8800 Clone`_, complete with toggle switches, blinky red
LEDs, etc.

Beginners may find `Patrick Jackson's videos`_ on YouTube quite
informative.

In addition to the conversion from `scanned printout`_ to searchable
HTML, this version contains corrections to typos in the original
document, as well as addtional material, particularly regarding the
`half adder`_ circuit, `instructions on assembling and loading code
from a Linux system`_, and a `fast forward to 2020`_ that includes
**"Hello World"** in both **GNU Assembler (gas)** and **Netwide
Assembler (nasm)**.

For a deeper dive into the Intel 8080 CPU, see the `Intel 8080
Programming Manual`_ which is unlikely to get converted into HTML by
me, alas. It's a *bit* |wink| too far.

----

Conversion from the scanned printout from PDF to HTML (and eventually,
a better PDF) was done with **pdftoppm** (one of the `poppler-utils`_
tools), `Tesseract-OCR`_, `Inkscape`_, `Sphinx`_ using the `Cloud`_
theme, and, of course, the `One True Editor`_ ("All Hail!") |wink|.

The printout itself came from a gold mine: `Dave Dunfield's`_ page
`Daves Old Computers`_ on the `Classic Computing`_ site. Serious
students and enthusiasts would be well-advised to put on their hard
hats and do some mining there. (Canary optional. |wink|)

The source and the rendered HTML are hosted on **Codeberg.org**, because
I **really** like their `mission statement`_

If you spot a problem, you can file an `issue`_.

Some effort was made to contact the copyright holder(s), now that
Micro Instrumentation and Telemetry Systems (MITS), Inc. is no more,
but alas, the search was fruitless.

In any case, the original is Copyright |cpyrgt| 1975, `MITS, Inc`_.

----

.. _Computer Systems course: https://ict.gctaa.net/sections/csc215/
.. _poppler-utils: https://en.wikipedia.org/wiki/Poppler_(software)#poppler-utils
.. _Tesseract-OCR: https://en.wikipedia.org/wiki/Tesseract_(software)
.. _Sphinx: https://en.wikipedia.org/wiki/Sphinx_(documentation_generator)
.. _Cloud: https://cloud-sptheme.readthedocs.io/en/latest/cloud_theme.html
.. _Inkscape: https://en.wikipedia.org/wiki/Inkscape
.. _One True Editor: https://en.wikipedia.org/wiki/Editor_war#Humor
.. _half adder: https://en.wikipedia.org/wiki/Adder_(electronics)#Half_adder
.. _MITS, Inc: https://en.wikipedia.org/wiki/Micro_Instrumentation_and_Telemetry_Systems
.. _MITS Altair 8800 simulator: https://www.s2js.com/altair/
.. _Altair 8800 Clone: https://altairclone.com/
.. _scanned printout: http://dunfield.classiccmp.org/altair/d/88opman.pdf
.. _Classic Computing: https://classiccmp.org/
.. _Daves Old Computers: http://dunfield.classiccmp.org/
.. _Dave Dunfield's: https://dunfield.themindfactory.com/
.. _Intel 8080 Programming Manual: https://altairclone.com/downloads/manuals/8080%20Programmers%20Manual.pdf
.. _instructions on assembling and loading code from a Linux system: linux.html
.. _fast forward to 2020: addendum.html
.. _issue: https://codeberg.org/ubuntourist/Altair8800/issues
.. _mission statement: https://blog.codeberg.org/codebergorg-launched.html
.. _Patrick Jackson's videos: https://www.youtube.com/playlist?list=PLgpana-oqo-KSE-Hlc3uLJ9xISCByBpO1
