.. Altair 8800 Operator's Manual documentation master file, created by
   sphinx-quickstart on Mon Sep 14 09:39:11 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. include:: /_static/isonum.txt

Altair 8800 Operator's Manual
=============================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   part-0
   part-1
   part-2
   part-3
   part-4
   appendix
   linux
   addendum
   resources
   service
   price-list

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
