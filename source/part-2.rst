.. include:: /_static/isonum.txt

.. include:: <isoamsa.txt>

.. role:: raw-html(raw)
   :format: html

#######################################
PART 2: ORGANIZATION OF THE ALTAIR 8800
#######################################

A block diagram showing the organization of the *ALTAIR 8800* is shown
in Figure 2-1. It is not necessary to understand the detailed
electronic operation of each part of the computer to make effective
use of the machine. However, a general understanding of each of the
various operating sections is important.

.. container:: center

   .. table:: FIGURE 2-1: Block diagram - Altair 8800 organization

      +------------------------------+
      | .. image:: _static/block.svg |
      +------------------------------+

.. container:: center

   .. table:: FIGURE 2-2: CPU Diagram

      +------------------------------+
      | .. image:: _static/CPU.svg   |
      |    :target: _static/CPU.html |
      +------------------------------+

(Click CPU Diagram for a larger image.)

A. CENTRAL PROCESSING UNIT (CPU)
================================

The Central Processing Unit (CPU) performs all arithmetic
calculations, makes all logical decisions, controls access to the
computer by input and output devices, stores and retrieves data from
the memory, and coordinates the orderly execution of a program. The
CPU is quite literally the heart of the computer.

Of course it is important to remember that the CPU is only as
intelligent as the programmer, for the CPU must be instructed in
precise terms just how to perform a particular operation. But since
the CPU in the *ALTAIR 8800* can execute a complete instruction cycle in
only 2 microseconds*, the computer can solve a highly complex problem
in an incredibly brief time. In fact, the *ALTAIR 8800* can execute a
six instruction addition program approximately 30,000 times in one
second.

The compact size and economy of the *ALTAIR 8800* is in large part due
to the CPU. Thanks to large scale integrated circuit techniques (LSI),
the CPU used in the *ALTAIR 8800* is fabricated on a tiny silicon chip
having a surface area of only a fraction of an inch. This chip, the
Intel 8080, is installed in a protective dual-in-line mounting package
having 40 pins.

The CPU is by far the most complex portion of the *ALTAIR 8800*. A
complete block diagram of the CPU is shown in Figure 2-2, and while it
is not necessary to possess a detailed understanding of this diagram
it is important to understand the role of some of the CPU's more
important systems. The interrelationship of each of these systems and
their contribution to the operation of the CPU will then become more
obvious.

1. TIMING AND CONTROL\ |--|\ The timing and Control System receives
   timing signals from the clock and distributes them to the
   appropriate portions of the CPU in order to insure coordinated
   instruction execution. The Timing and Control System also activates
   several front panel status indicators (HOLD, WAIT, INTE, STACK,
   OUT, IN, INP, MI MENR, HLTA, WO, INT).

   * A microsecond is one millionth of a second.

2. INSTRUCTION REGISTER\ |--|\ Binary machine language instructions
   are temporarily stored in the Instruction Register for decoding
   and execution by the CPU.

3. ARITHMETIC\ |--|\ The Arithmetic System performs both binary and
   decimal arithmetic. All arithmetic operations are performed by
   addition. Multiplication is implemented by repetitive addition.
   Subtraction and division are implemented by inverse addition.

4. WORKING REGISTERS\ |--|\ The CPU contains seven 8-bit Working
   Registers. The most important of these is the Accumulator, the
   register into which the results of many operations are eventually
   loaded. In addition to acting as a primary storage point for
   results of many program operations, numerous arithmetic and logical
   operations can be performed with the Accumulator and any specified
   register or memory address,

   The six remaining registers, which are arranged in pairs to permit
   16-bit operation when necessary, are "scratchpad" registers. This
   simply means they are used to store temporary data or addresses on
   a regular basis and are available for numerous program operations.

   Figure 2-3 shows the arrangement and classification of the seven
   Working Registers. The additional register adjacent to the
   Accumulator, the Status Bit Register, is a special purpose register
   used to store the status of certain operations.

.. container:: center

   .. table:: FIGURE 2-3. The Working Registers
      :widths: 2 1 1 1
      :column-alignment: left left center center
      :column-dividers:  none none single single single

      +-------------------+---------+-------+-------+
      | Register Pair B   | |xrarr| | ``B`` | ``C`` |
      +-------------------+---------+-------+-------+
      | Register Pair D   | |xrarr| | ``D`` | ``E`` |
      +-------------------+---------+-------+-------+
      | Register Pair H   | |xrarr| | ``H`` | ``L`` |
      +-------------------+---------+-------+-------+
      | Register Pair PSW | |xrarr| | ``*`` | ``A`` |
      +-------------------+---------+-------+-------+

``*`` Status Bit Register (See Text)


5. STATUS BIT REGISTER\ |--|\ The Status Bit Register is a special
   purpose register which stores the status of five conditions which
   may or may not be affected by the result of a data operation. This
   register contains 8-bit positions, but only 5-bits are used to
   store the status information.  The five status bits are:

   a. Carry Bit\ |--|\ This bit is set to 1 if a carry has occurred.
      The Carry Bit is usually affected by such operations as addition,
      subtraction, rotation, and some logical decisions. The bit is set
      to 0 if no carry occurs.

   b. Auxiliary Carry Bit\ |--|\ If set to 1, this bit indicates a
      carry out of bit 3 of a result. 0 indicates no carry. This
      status bit is affected by only one instruction (DAA).

   c. Sign Bit\ |--|\ This bit is set to show the sign of a result.
      If set to 1, the result is minus; if set to 0 the result is
      plus. The Sign Bit reflects the condition of the most
      significant bit in the result (bit 7). This is because an
      8-bit byte can contain up to the decimal equivalent of from
      -128 to +127 if the most significant bit is used to indicate
      the polarity of the result. 23

   d. Zero Bit\ |--|\ This bit is set to 1 if the result of certain
      instructions is zero and reset to 0 if the result is greater
      than zero.

   e. Parity Bit\ |--|\ Certain operations check the parity of the
      result. Parity indicates the odd or even status of the 1 bits
      in the result. Thus if there is an even number of 1 bits, the
      Parity Bit is set to 1, and if there is an odd number of 1 bits,
      the Parity Bit is set to 0.

6. PROGRAM COUNTER\ |--|\ The Program Counter is a special 16-bit
   register which stores the address of the next program step to be
   executed.  The Program Counter is automatically advanced to the
   next sequential program address upon completion of a step
   execution.  Sometimes called the P-Counter, the Program Counter
   is directly accessible to the programmer via machine language
   instructions which implement JUMP, CALL, and RETURN instructions.

7. STACK POINTER\ |--|\ The Stack Pointer is another special 16-bit
   register. A section of memory reserved for the temporary storage
   of data or addresses is called the stack.

   Data can be pushed onto the stack for temporary storage and popped
   out of the stack via several instructions.

   The Stack Pointer is used to store the contents of the Program
   Counter during the execution of subroutines. A RETURN instruction
   transfers the contents of the Stack Pointer to the Program Counter
   and sequential execution of the main program continues. The
   programmer selects the location of the stack in memory by loading
   the Stack Pointer with the desired memory address via a special
   instrution (LXI).

The interrelationship of the Working Registers, Program Counter, Stack
Pointer, Arithmetic System, Instruction Register, and Timing and
Control System should now be more meaningful. The Working Registers
incorporate six scratchpad registers and an Accumulator into which
numerous operation results are temporarily stored. The Program Counter
causes sequential execution of a program by keeping track of the
memory address of the next instruction to be executed. The Timing and
Control System supplies timing pulses which coordinate orderly program
execution. The Stack Pointer is used for temporary storage of the data
contained in any register pair. The Stack Pointer also saves the
address in the Program Counter for retrieval after a subroutine has
been executed. All these operations combine to provide an enormously
flexible and versatile CPU.

B. MEMORY
=========

Though the Working Registers, Program Counter, and Stack Pointer
certainly perform memory roles, the CPU does not contain memory as it
is normally defined in a computer application. The primary memory in a
computer is external to the CPU.

Simple programs can be implemented with a few dozen words of memory or
even less, but more complex applications such as video processing
require more memory. The *ALTAIR 8800* is expandable to 65,536 8-bit
words of memory.

Access to the memory is always controlled by the CPU.* 16 address
lines called the Address Bus connect the CPU to the Memory. These
lines permit the CPU to input or output data to or from any memory
address. The addresses are specified by two 8-bit bytes. The CPU
processes each address as two sequential (serial) cycles, each
containing 8-parallel bits. Data stored in the Memory is exchanged
between the Memory and CPU via 8 data lines called the Data Bus. This
interconnection format permits parallel operation. Thus, when data is
read from or written to Memory by the CPU, it is transmitted as a
complete 8-bit word.

The basic Memory in the *ALTAIR 8800* contains up to eight 256 x 4 bit
random access memories (RAMS). However, any conventional memory can be
used in the computer if input loading on the buss does not exceed 50
TTL loads and if the buss is driven by standard TTL loads.

* An exception to this is when the computer is connected to a Direct
  Memory Access Controller. DMA takes control of the address and data
  lines from the CPU for direct transfers of blocks of data. These
  transfers can take place internally (from one memory location to
  another) or externally (from memory to an external device).

C. CLOCK
========

Orderly execution of a program by the CPU is controlled by a 2-MHz
crystal controlled clock. Crystal control is used to permit the clock
to operate at the maximum permissible CPU speed. A clock without
crystal regulation might occassionally speed up beyond the CPU's
capability and program execution errors would result.

D. INPUT/OUTPUT
===============

The *ALTAIR 8800* can be interfaced with a great many external
devices. Generally, these devices provide input information to the
computer and accept output information from the computer. The CPU
monitors the status of program execution and Input/Output devices and
provides the necessary signals for servicing external devices. The
programmer can instruct the CPU to either ignore or respond to
interrupt signals provided by an external device. These interrupt
signals, when accepted by the CPU, cause the program execution to be
temporarily halted while the external device is serviced by the
computer. When the external device has been serviced, the program
resumes normal execution.  The *ALTAIR 8800* will service up to 256
Input and 256 Output devices.

This concludes the description of the organization of the *ALTAIR
8800*. The overall operation of the computer as a powerful and
efficient data processing system will become more apparent in Part 3,
a discussion of the operation of the *ALTAIR 8800*.
