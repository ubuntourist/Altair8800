#########
RESOURCES
#########

If you are the owner of an Altair 8800 Clone, I recommend `Altair
Clone Operator's Manual`_ as well as other resources on the `Support`_
page. In particular **PART 2 – CONFIGURATION MONITOR** which provides
details for easily loading ``.BIN`` and ``.HEX`` files.

There is a wealth of information from `Dave Dunfield`_ on his
`Classic Computing`_ page, which is where we obtained:

* `Assembly Language Programming System (A.L.P.S.)`_
* `Device Management Facility (DMF-80/ST)`_

And, the Atari Clone downloads. particularly the `Manuals`_ page,
source of:

* `4kbootstrap.pdf`_
* `Altair 2SIO Serial I-O.pdf`_
* `Microsoft M80 Assembler.pdf`_

Also, the `Altair Front Panel`_ page has several useful program examples.

One `Jeff Tranter`_ provided tidbits on the `Macroassembler AS`_ which
has to be compiled from source, but apparently gets the job done.

----

.. _Altair Clone Operator's Manual: https://altairclone.com/downloads/manuals/Altair%20Clone%20Operator's%20Manual.pdf
.. _Support: https://altairclone.com/support.html

.. _Dave Dunfield: https://dunfield.themindfactory.com/

.. _Classic Computing: https://www.classiccmp.org/dunfield/altair/

.. _Manuals: https://altairclone.com/downloads/manuals/

.. _Altair Front Panel: https://altairclone.com/downloads/front_panel/

.. _Jeff Tranter: https://jefftranter.blogspot.com/2014/03/the-as-macro-assembler.html

.. _Macroassembler AS: http://john.ccac.rwth-aachen.de:8000/as/index.html

.. _Assembly Language Programming System (A.L.P.S.): _static/alps.txt
.. _Device Management Facility (DMF-80/ST): _static/dmf.txt

.. _4kbootstrap.pdf: _static/4kbootstrap.pdf
.. _Altair 2SIO Serial I-O.pdf: https://altairclone.com/downloads/manuals/Altair%202SIO%20Serial%20I-O.pdf
.. _8080 Programmers Manual.pdf: https://altairclone.com/downloads/manuals/8080%20Programmers%20Manual.pdf
.. _Microsoft M80 Assembler.pdf: _static/Microsoft%20M80%20Assembler.pdf
